<?php

namespace Mintos\Classes;

use \Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Mintos\Exceptions\CannotLoadFeedFromHTTPException;
use Mintos\Exceptions\InvalidXMLException;
use Mintos\Interfaces\ReaderInterface;
use SimpleXMLElement;

class Reader implements ReaderInterface
{
    /**
     * @var string
     */
    private string $feedURL;

    /**
     * @var string
     */
    private string $cacheKey;

    /**
     * @var bool
     */
    private bool $enableCaching = false;

    /**
     * @var int
     */
    private int $cacheExpiry = 0;

    /**
     * @var Client
     */
    private Client $client;

    /**
     * @param Client $client instance of GuzzleClient
     * @param string $feedURL feed url
     * @param bool $enableCaching boolean flag on whether to cache the feed result
     * @param int $ttl cache TTL in seconds
     */
    public function __construct(Client $client, string $feedURL, bool $enableCaching = true, int $ttl = 3600)
    {
        $this->client = $client;

        $this->feedURL = $feedURL;
        $this->cacheKey = base64_encode($feedURL);
        $this->enableCaching = $enableCaching;
        $this->cacheExpiry = $ttl;

        // setting TTL to 0 will forget an existing cached feed (if any)
        if (!$enableCaching) {
            $this->cacheExpiry = 0;
        }
    }

    /**
     * @param bool $fresh
     * @return array
     * @throws CannotLoadFeedFromHTTPException
     * @throws GuzzleException
     * @throws InvalidXMLException
     */
    public function loadFeed(bool $fresh = false): array
    {
        // try to retrieve an item from cache, if we're not explicitly requesting a fresh feed, and caching is enabled
        if (!$fresh && $this->enableCaching && $this->isCached()) {
            return $this->getFeedFromCache();
        }

        // Load the XML from HTTP
        $feed = $this->getFeedFromHTTP();

        // Convert to array
        $feed = $this->toArray($feed);

        // Cache the result
        $this->cacheFeed($feed);

        return $feed;
    }

    /**
     * @return bool
     */
    private function isCached(): bool
    {
        return Cache::has($this->cacheKey);
    }

    /**
     * @return array
     */
    private function getFeedFromCache(): array
    {
        return Cache::get($this->cacheKey);
    }

    /**
     * @return SimpleXMLElement
     * @throws CannotLoadFeedFromHTTPException
     * @throws InvalidXMLException
     * @throws GuzzleException
     */
    private function getFeedFromHTTP(): SimpleXMLElement
    {
        try {
            $response = $this->client->get($this->feedURL);
            $response = $response->getBody()->getContents();
        } catch (Exception $e) {
            throw new CannotLoadFeedFromHTTPException;
        }

        try {
            $simpleXML = new SimpleXMLElement($response);
        } catch (Exception $e) {
            throw new InvalidXMLException;
        }

        return $simpleXML;
    }

    /**
     * @param array $feed
     * @return bool
     */
    private function cacheFeed(array $feed): bool
    {
        return Cache::put($this->cacheKey, $feed, $this->cacheExpiry);
    }

    /**
     * Converts SimpleXML Object to Array
     *
     * @param SimpleXMLElement $xml
     * @return array
     */
    private function toArray(SimpleXMLElement $xml): array
    {
        // Apparently this is the best way of converting XML to something that can actually be worked with in PHP
        return json_decode(json_encode((array)$xml), true);
    }
}
