<?php

namespace Mintos\Interfaces;

use GuzzleHttp\Client;

interface ReaderInterface
{
    /**
     * @param Client $client instance of GuzzleClient
     * @param string $feedURL feed url
     * @param bool $enableCaching boolean flag on whether to cache the feed result
     * @param int $ttl cache TTL in seconds
     */
    public function __construct(Client $client, string $feedURL, bool $enableCaching = true, int $ttl = 3600);

    /**
     * Load the Feed.
     *
     * @param bool $fresh flag whether to retrieve a fresh feed, ignoring the cached record, if any
     * @return array
     */
    public function loadFeed(bool $fresh = false): array;
}
