<?php

namespace Mintos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidationController extends Controller
{
    public function post(Request $request)
    {
        $request->ajax();
        $requestData = $request->all();

        $validatedData = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
    }
}
