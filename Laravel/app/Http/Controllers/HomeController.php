<?php

namespace Mintos\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Mintos\Exceptions\CannotLoadFeedFromHTTPException;
use Mintos\Exceptions\InvalidXMLException;
use Mintos\Helpers\RSSFeedHelper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     * @throws GuzzleException
     * @throws CannotLoadFeedFromHTTPException
     * @throws InvalidXMLException
     */
    public function index()
    {
        $feedURL = 'https://www.theregister.com/software/headlines.atom';
        $feedHelper = new RSSFeedHelper($feedURL);

        $topWords = $feedHelper->getTopWords();
        $feed = $feedHelper->getFeed();

        return view('home', [
            'topWords' => $topWords,
            'feed' => $feed,
        ]);
    }
}
