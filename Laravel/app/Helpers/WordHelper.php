<?php

namespace Mintos\Helpers;

class WordHelper
{
    /**
     * @param array $wordsToLookFor
     * @param array $arrayToLookIn
     * @param array $keysToLookIn
     * @return array
     */
    public static function getWordCount(array $wordsToLookFor, array $arrayToLookIn, array $keysToLookIn): array
    {
        $foundWords = [];
        $wallOfText = '';


        foreach ($arrayToLookIn as $i => $array) {
            foreach ($keysToLookIn as $key) {
                if (isset($array[$key])) {
                    $text = $array[$key];
                    $text = self::cleanString($text);
                    $wallOfText .= ' ' . $text;
                }
            }
        }

        $words = explode(' ', $wallOfText);

        foreach ($words as $word) {
            if (in_array($word, $wordsToLookFor)) {
                if (!isset($foundWords[$word])) {
                    $foundWords[$word] = 1;
                } else {
                    $foundWords[$word] += 1;
                }
            }
        }

        arsort($foundWords, SORT_NUMERIC);

        return $foundWords;
    }

    /**
     * @param string $string
     * @return string
     */
    private static function cleanString(string $string): string
    {
        // strip all html tags
        $string = strip_tags($string);

        // allow only letters
        $string = preg_replace('/[^a-zA-Z\ ]/', '', $string);

        // make lowercase
        $string = strtolower($string);

        // trim
        $string = trim($string);

        return $string;
    }
}
