<?php

namespace Mintos\Helpers;

class Top50EnglishWords
{
    /**
     * Ignoring plurals and other grammatical cases of potentially the same word is a deliberate design choice
     * due to the fact that it opens up too many edge cases that need to be addressed, and it also opens up a can of worms about what
     * we consider to be the same word. Are all grammatical cases of the same word still the same word?
     * What about noun "will" and a verb "will"? Is it still the same word?
     *
     * For the same exact reason I made the design choice to ignore 's cases
     *
     * @url https://xkcd.com/1425/ - relevant xkcd
     */
    const WORDS = [
        'the',
        'be',
        'to',
        'of',
        'and',
        'a',
        'in',
        'that',
        'have',
        'I',
        'it',
        'for',
        'not',
        'on',
        'with',
        'he',
        'as',
        'you',
        'do',
        'at',
        'this',
        'but',
        'his',
        'by',
        'from',
        'they',
        'we',
        'say',
        'her',
        'she',
        'or',
        'an',
        'will',
        'my',
        'one',
        'all',
        'would',
        'there',
        'their',
        'what',
        'so',
        'up',
        'out',
        'if',
        'about',
        'who',
        'get',
        'which',
        'go',
        'me',
    ];
}
