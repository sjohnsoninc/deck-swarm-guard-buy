<?php

namespace Mintos\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Mintos\Classes\Reader;
use Mintos\Exceptions\CannotLoadFeedFromHTTPException;
use Mintos\Exceptions\InvalidXMLException;

class RSSFeedHelper
{
    /**
     * @var string RSS Feed URL
     */
    private string $feedURL;

    private array $feed;

    public function __construct(string $feedURL)
    {
        $this->feedURL = $feedURL;
    }

    /**
     * @return array
     * @throws GuzzleException
     * @throws CannotLoadFeedFromHTTPException
     * @throws InvalidXMLException
     */
    public function getFeed(): array
    {
        if (isset($this->feed)) {
            return $this->feed;
        }

        $reader = new Reader(new Client, $this->feedURL);
        $feed = $reader->loadFeed();
        $feed = $feed['entry'];
        $this->feed = $feed;

        return $this->feed;
    }

    /**
     * @return array
     * @throws CannotLoadFeedFromHTTPException
     * @throws GuzzleException
     * @throws InvalidXMLException
     */
    public function getTopWords(): array
    {
        $feed = $this->getFeed();
        $topWords = WordHelper::getWordCount(Top50EnglishWords::WORDS, $feed, ['title', 'summary']);
        $top10 = array_slice($topWords, 0, 9, true);

        return $top10;
    }
}
