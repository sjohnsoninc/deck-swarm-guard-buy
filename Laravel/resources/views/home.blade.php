@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Top 10 Words in the Feed') }}</div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('Word') }}</th>
                                <th scope="col">{{ __('Count') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($topWords as $topWord => $instances)

                                <tr>
                                    <td>{{ $topWord }}</td>
                                    <td>{{ $instances }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card" style="margin-top: 20px">
                    <div class="card-header">{{ __('Feed Items') }}</div>
                    <div class="card-body">
                        @foreach($feed as $item)
                            <div class="card" style="margin-top: 20px">
                                <div class="card-header"><a href="{{ $item['link']['@attributes']['href'] }}">{{ $item['title']  }}</a></div>
                                <div class="card-body">
                                    {{ __('Author') }}: <a href="{{ $item['author']['uri'] }}">{{ $item['author']['name'] }}</a><br>
                                    {{ strip_tags($item['summary']) }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
